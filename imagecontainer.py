class ImageContainer:
    def __init__(self, images, submission_url):
        assert isinstance(images, list)
        assert isinstance(submission_url, str)
        self.submission_url = submission_url
        self.images = images
