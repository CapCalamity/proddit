from imagecontainer import ImageContainer

from praw import Reddit, objects, errors
from imgurpython import ImgurClient
from datetime import datetime
from urllib import request
from queue import Queue
import os
import shutil
import re
import argparse
import threading
import time


class Controller:
    imgurClientId = 'de388ca6d25724f'
    imgurClientSecret = 'cd140d85e2c0e897e3828d61896c2221ac0800c1'

    def __init__(self, subreddits=[], image_folder="",
                 history_file="", number=20):

        print("initializing...")

        user_agent = "Python:Proddit:v0.1 (by /u/killadrake000"

        self.subreddits = subreddits
        self.fetch_threads = []
        self.reddit = Reddit(user_agent=user_agent)
        self.imgurClient = ImgurClient(self.imgurClientId,
                                       self.imgurClientSecret)
        self.re_imgurAlbumId = r'.com/a/([\w]+)[/#]?.*'
        self.re_imgurImageId = r'.com/([\w]+)[/#]?.*'
        self.image_folder = image_folder
        self.history_file = history_file
        self.completed_submissions = []
        self.submission_queue = Queue()
        self.num_posts = number

        self.load_completed_submissions()

        try:
            if not os.path.exists(self.image_folder):
                os.makedirs(self.image_folder)
        except OSError:
            print("could not create necessary folder {0}".format(
                self.image_folder))
            exit(2)

    def load_completed_submissions(self):
        print("loading already completed submissions...")
        if os.path.exists(self.history_file):
            with open(self.history_file, "r") as tmpFile:
                for line in tmpFile:
                    line = line.strip()
                    if line != "":
                        self.completed_submissions.append(line)

    def start(self) -> None:
        for sub in self.subreddits:
            thread = threading.Thread(target=self.pull_images,
                                      daemon=True,
                                      args=[sub])
            thread.start()
            self.fetch_threads.append(thread)

        while len(self.fetch_threads) > 0:
            try:
                while not self.submission_queue.empty():
                    container = self.submission_queue.get(block=False)
                    self.download_image(container)
                self.fetch_threads =
                [t for thread in self.fetch_threads if t.isAlive()]
            except:
                print("Exception occured during download")
            time.sleep(2)

    def pull_images(self, reddit) -> None:
        try:
            assert isinstance(self.reddit, Reddit)
            sub = self.reddit.get_subreddit(reddit)
            print(sub.display_name)
            for submission in sub.get_hot(limit=self.num_posts):
                try:
                    while self.submission_queue.qsize() > len(self.subreddits):
                        time.sleep(2)

                    if self.already_downloaded(submission.url):
                        # print("-|{0:<9}:{1:<30}:{2:<30}".format(submission.fullname,
                        # submission.url[:30],
                        # submission.title[:30]).strip())

                        continue
                    else:
                        assert isinstance(submission, objects.Submission)
                        print("+|{0:<9}:{1:<30}:{2:<30}"
                              .format(submission.fullname,
                                      submission.url[:30],
                                      submission.title[:30]).strip())

                    url = submission.url.lower()
                    assert isinstance(url, str)

                    if url.find("imgur.com") != -1:
                        if url.find("/a/") != -1:
                            match = re.search(self.re_imgurAlbumId,
                                              submission.url)
                            if match:
                                id = match.group(1)
                                images = []
                                album_index = 1
                                for img in self.imgurClient.get_album_images(id):
                                    images.append((img.link,
                                                   "{0}/{1}/{2}-{3}"
                                                   .format(sub.display_name,
                                                           id,
                                                           album_index,
                                                           img.link.split("/")[-1])))
                                    album_index += 1

                                self.queue_image(
                                    ImageContainer(images,
                                                   submission.url))
                                print("{0}:is album, {1} images found"
                                      .format(submission.fullname,
                                              len(images)))
                            else:
                                print("{0}:could not locate gallery id..."
                                      .format(submission.fullname))
                        else:
                            match = re.search(self.re_imgurImageId,
                                              submission.url)
                            if match:
                                id = match.group(1)
                                img = self.imgurClient.get_image(id)
                                self.queue_image(
                                    ImageContainer(
                                        [(img.link,
                                            "{0}/{1}"
                                            .format(sub.display_name,
                                                    img.link.split("/")[-1]))],
                                        submission.url))
                                print("{0}:is single imgur image"
                                      .format(submission.fullname))
                            else:
                                print("{0}:url could not be resolved"
                                      .format(submission.fullname))

                    elif url.endswith(".jpg") \
                            or url.endswith(".jpeg") \
                            or url.endswith(".bmp") \
                            or url.endswith(".png") \
                            or url.endswith(".gif"):
                        print("{0}:url is deeplink"
                              .format(submission.fullname))
                        self.queue_image(
                            ImageContainer([(submission.url,
                                           "{0}/{1}"
                                            .format(sub.display_name,
                                                    url.split("/")[-1]))],
                                           submission.url))

                    else:
                        print("{0}:could not be resolved, unknown format"
                              .format(submission.fullname))

                    time.sleep(2)
                except errors.RateLimitExceeded:
                    pass
                except errors.APIException:
                    pass
                except Exception:
                    print("exception occured")

        except errors.RateLimitExceeded:
            print("Rate Limit Exception occured in {0}".format(reddit))
        except errors.APIException:
            print("API Exception occured in {0}".format(reddit))

    def add_to_history(self, submission):
        with open(self.history_file, "a") as tmpFile:
            tmpFile.write("{0}\n".format(submission))

    def already_downloaded(self, submission_url) -> bool:
        for submission in self.completed_submissions:
            if submission == submission_url:
                return True
        return False

    def queue_image(self, container):
        assert isinstance(container, ImageContainer)
        self.submission_queue.put(container)

    def download_image(self, container):
        assert isinstance(container, ImageContainer)

        ClientRemaining = self.imgurClient.credits["ClientRemaining"]
        ClientLimit = self.imgurClient.credits["ClientLimit"]
        UserRemaining = self.imgurClient.credits["UserRemaining"]
        UserLimit = self.imgurClient.credits["UserLimit"]
        UserReset = self.imgurClient.credits["UserReset"]

        print("Client-Tokens: {0}/{1}".format(ClientRemaining, ClientLimit))

        print("User-Tokens: {0}/{1}".format(UserRemaining, UserLimit))

        print("Time to Reset: {0}/{1}"
              .format(UserReset,
                      datetime.fromtimestamp(int(UserReset))
                      .strftime("%Y/%m/%d-%H:%M:%S")))
        print("")

        for image in container.images:
            (url, filename) = image
            path = "/".join(filename.split("/")[:-1])
            if not os.path.exists(self.image_folder + path):
                os.makedirs(self.image_folder + path)
            with request.urlopen(url) as response, open(self.image_folder +
                                                        filename,
                                                        'wb') as out_file:
                shutil.copyfileobj(response, out_file)
            time.sleep(1)

        self.add_to_history(container.submission_url)


parser = argparse.ArgumentParser(description='Download images from Reddit')

parser.add_argument('-s', '--subreddit',
                    help="subreddit url e.g. 'askreddit'",
                    nargs='+',
                    type=str)

parser.add_argument('-p', '--path',
                    default="./files/",
                    nargs='?',
                    type=str)

parser.add_argument('-f', '--history',
                    default="./history",
                    nargs='?',
                    type=str)

parser.add_argument('-n', '--number',
                    default=20,
                    nargs='?',
                    type=int)

args = parser.parse_args()

controller = Controller(subreddits=args.subreddit,
                        image_folder=args.path,
                        history_file=args.history,
                        number=args.number)
controller.start()

input("enter to close program")
